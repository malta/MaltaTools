#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import os
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
from MaltaBase import *
import MaltaSetup
import Herakles
import PyMaltaSlowControl

parser=argparse.ArgumentParser()
parser.add_argument('-dc','--doublecolumn',help="List of double columns to mask",nargs='+',type=int)
parser.add_argument('-f','--first',help="First double column",nargs='+',type=int)
parser.add_argument('-l','--last',help="Last double column",nargs='+',type=int)
parser.add_argument('-s','--sample',help="Sample name")
args=parser.parse_args()

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################

connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)

GetVersion(ipb,True)
print GetStatus(ipb)
getFifoState(ipb)
ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

#os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
#os.system("MALTA_PSU.py -t T_SC_ON.txt ")

###################################
############# starting the SC clock
###################################
switchClockOn(ipb)

############# MaltaSlowControl instance
sc=PyMaltaSlowControl.MaltaSlowControl()

'''
dcs=[]
if args.first and args.last:
    if args.first >= args.last:
        print "First DC cannot be smaller than last: %i %i" % (args.first, args.last)
        pass
    dcs.extend(xrange(args.first,args.last+1))
    pass

for dc in args.doublecolumn:
    if not dc in dcs: dcs.append(dc)
    pass

samples={"W12R3":[0,36],
         "W4R2":[3,47,57,77,80,81,82,121],
         "W4R10":[50,54,58,97,21,39,101,104],
         "W4R12":[9,15,19,26,79,98,101,110,112,121],
         "W4R20":[21,39,50,54,58,101,104],
         "W12R24":[4,16,46,53,54,55,58,69,70,86,104,118],
         "W12R11":[9,15,19,26,79,98,101,110,112,121],
         }        

if args.sample:
    if not args.sample in samples:
        print "Could not find sample: %s" % args.sample
        print "Available samples:"
        for s in samples: print "  %s" % s
        pass
    else:
        for dc in samples[args.sample]:
            if not dc in dcs: dcs.append(dc)
            pass
        pass
    pass

for dc in dcs:
    ipb.Write(5, sc.maskDoubleColumn(dc))
    time.sleep(0.1)
    pass

'''
os.system("MALTA_PSU.py -c rampVoltage DVDD 0.9 0.4")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")



#for dc in xrange(92,130):
#for dc in xrange(0,64):
#for dc in xrange(126,255):
''' 
for dc in xrange(2,255,4):
    print ("Masking DC" + str(dc))
    command= sc.maskDoubleColumn(dc)
    #command= sc.readRegister(24)
    ################print command
    #ipb.Write(5,0xff99)
    ipb.Write(5,command)
    #ipb.Write(5,0xfa00)
    time.sleep(0.05)
    ipb.Write(5, command)
    time.sleep(0.05)
    #ipb.Write(5, command)
    #time.sleep(0.001)
    #ipb.Write(5, command)
    #time.sleep(0.001)
    #ipb.Write(5, command)
    #time.sleep(0.001)
    DebugWord(ipb.Read(4), 0)
#    for i in xrange(1,100):
#        ipb.Write(5, command)
#        time.sleep(0.01)
#        pass
    pass
 
for dc in xrange(3,255,4):
    print ("Masking DC" + str(dc))
    command= sc.maskDoubleColumn(dc)
    #command= sc.readRegister(24)
    ################print command
    #ipb.Write(5,0xff99)
    ipb.Write(5,command)
    #ipb.Write(5,0xfa00)
    time.sleep(0.05)
    ipb.Write(5, command)
    time.sleep(0.05)
    #ipb.Write(5, command)
    #time.sleep(0.001)
    #ipb.Write(5, command)
    #time.sleep(0.001)
    #ipb.Write(5, command)
    #time.sleep(0.001)
    DebugWord(ipb.Read(4), 0)
#    for i in xrange(1,100):
#        ipb.Write(5, command)
#        time.sleep(0.01)
#        pass
    pass
 
for dc in xrange(0, 512):
    command= sc.maskPixelCol(dc)
    print command
    ipb.Write(5, command)
    DebugWord(ipb.Read(4), 0)
    time.sleep(0.001)
    pass

for dc in xrange(0, 512):
    command= sc.maskPixelHor(dc)
    print command
    ipb.Write(5, command)
    DebugWord(ipb.Read(4), 0)
    time.sleep(0.001)
    pass



for dc in xrange(0, 512):
    command= sc.maskPixelDiag(dc)
    print command
    ipb.Write(5, command)
    DebugWord(ipb.Read(4), 0)
    time.sleep(0.001)
    pass

   

#The following arrays contain the coordinates of the pixels obtained with the scrip NTupleMaker while doing a noise scan and that we would like to mask for the chip W7R12 
pixelX_tomask = [76,  168, 287, 289, 298, 311, 318, 319, 320, 328, 334, 367, 367, 382, 399, 414, 414, 453, 465, 470, 474, 477, 485, 498, 498, 502, 502, 511]
pixelY_tomask = [101, 212, 231, 279, 109, 220, 151, 126, 475, 220, 227, 98,  448, 333, 131, 422, 444, 284, 217, 24,  390, 287, 328, 56 , 209, 110, 332, 254]

for pixX in pixelX_tomask:
    print ("Masking DC" + str(pixX/2))
    command= sc.maskDoubleColumn(pixX/2)
    ipb.Write(5,command)
    time.sleep(0.01)
    pass
    
    

for pixX in pixelX_tomask:
    command= sc.maskPixelHor(pixX)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX-6)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX-7)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX-8)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX-9)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX-10)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX+6)    
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX+7)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX+8)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX+9)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelHor(pixX+10)
    ipb.Write(5, command)
    time.sleep(0.01)
    #print str(pixX)
    #print command
    #DebugWord(ipb.Read(4), 0) 
    pass

for pixY in pixelY_tomask:
    command= sc.maskPixelCol(pixY)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY-6)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY-7)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY-8)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY-9)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY-10)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY+6)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY+7)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY+8)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY+9)
    ipb.Write(5, command)
    time.sleep(0.01)
    command= sc.maskPixelCol(pixY+10)
    ipb.Write(5, command)
    time.sleep(0.01)
   # print command
    #DebugWord(ipb.Read(4), 0)
    pass
    

for dc in xrange(0, 255):
    command= sc.maskPixelCol(dc)
    print command
    ipb.Write(5, command)
    DebugWord(ipb.Read(4), 0)
    time.sleep(0.01)
    pass

for dc in xrange(255, 512):
    command= sc.maskPixelHor(dc)
    print command
    ipb.Write(5, command)
    DebugWord(ipb.Read(4), 0)
    time.sleep(0.01)
    pass
'''
for dc in xrange(0, 512):
    command= sc.maskPixelDiag(dc)
    #print command
    ipb.Write(5, command)
    #DebugWord(ipb.Read(4), 0)
    time.sleep(0.01)
    pass

for i in xrange (0, 2):
    command = sc.maskPixelCol(1)
    ipb.Write(5, command)
    print command
    time.sleep(0.01)


for i in xrange (510, 512):
    command = sc.maskPixelHor(511)
    ipb.Write(5, command)
    print command
    time.sleep(0.01)
    
###################################
############# stopping the SC clock
###################################

time.sleep(1)


##time.sleep(100000000)

switchClockOff(ipb)
os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")

print "End of script.\n"
