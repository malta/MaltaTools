#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()

parser.add_argument('-c' ,'--chip'     , help='Which chip to talk to'  ,type=int ,default=1)
parser.add_argument('-r' ,'--remote'   , help='use remote PSU'         ,action="store_true")
parser.add_argument('-w' ,'--waterchip', help='Which chip to talk to'  ,action="store_true")
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')

args=parser.parse_args()
chip=args.chip
isMicrochannel=args.waterchip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

readA =4
writeA=5
if args.dualchip:
    readA =90
    writeA=91

if not isMicrochannel:
    os.system(name+" -t T_SC_ON.txt ")
    os.system(name+" -t T_ANAG_OFF.txt ")
else:
    os.system(name+" -t T_SC_ON_M.txt ")
    os.system(name+" -t T_ANAG_OFF_M.txt ")


connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)


if GetVersion(ipb,True)==0: sys.exit()
print ("Done")
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure"
ipb.Write(5, sc.enableMerger(False))
ipb.Write(5, sc.enableMerger(False))
ipb.Write(5, sc.enableMerger(False))
#ipb.Write(5, sc.enableMerger(True))
ipb.Write(5, sc.delay(750))
ipb.Write(5, sc.delay(750))
DebugWord(ipb.Read(4), 0)

############# DAC monitoring
#print "\n Disabling DAC monitoring"
ipb.Write(5, sc.switchDACMONI(False))
ipb.Write(5, sc.switchDACMONV(False))
#ipb.Write(5, sc.switchDACMONV(True))
#ipb.Write(5, sc.switchDACMONI(True))
ipb.Write(5, sc.readRegister(21)    )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
############# DAC override
#print "\n Switching on IDB"
ipb.Write(5, sc.switchIDB(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on ITHR"
ipb.Write(5, sc.switchITHR(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on IBIAS"
ipb.Write(5, sc.switchIBIAS(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on IRESET"
ipb.Write(5, sc.switchIRESET(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on ICASN"
ipb.Write(5, sc.switchICASN(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on VCASN"
ipb.Write(5, sc.switchVCASN(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on VCLIP"
ipb.Write(5, sc.switchVCLIP(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on VPLSE_HIGH"
ipb.Write(5, sc.switchVPLSE_HIGH(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on VPLSE_LOW"
ipb.Write(5, sc.switchVPLSE_LOW(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.05)
#print "\n Switching on VRESET_D"
ipb.Write(5, sc.switchVRESET_D(True) )
DebugWord(ipb.Read(4), 0)
#time.sleep(0.05)
#print "\n Switching on VRESET_P"
ipb.Write(5, sc.switchVRESET_P(True) )
DebugWord(ipb.Read(4), 0)

time.sleep(0.05)

#############
#ipb.Write(writeA, sc.setIBUFP_MON1(8) )
#ipb.Write(writeA, sc.setIBUFN_MON1(15) )
#ipb.Write(writeA, sc.setIBUFP_MON0(8) )
#ipb.Write(writeA, sc.setIBUFN_MON0(15) )
ipb.Write(writeA, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

#############LVDS stuff
#print "\n LVDS register before change:"
#ipb.Write(writeA,sc.readRegister(5))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)
#ipb.Write(writeA,sc.configHBridge(31))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)
#print "\n LVDS register after change:"
#ipb.Write(writeA,sc.readRegister(5))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)

print "\n Configuring masking and pulsing"

############# VD: disable everything that can produce noise in the chip
ipb.Write(writeA, sc.disablePulsePixelHor(0))
time.sleep(0.1)
ipb.Write(writeA, sc.disablePulsePixelCol(5))

for dc in range(0,64):
    ipb.Write(5, sc.maskDoubleColumn(dc)) ##uncommented from run 5288
    time.sleep(0.01)
    pass

if isMicrochannel:
    for dc in range(127,256):
        ipb.Write(5, sc.maskDoubleColumn(dc)) ##uncommented from run 5288
        time.sleep(0.01)
        pass

switchClockOff(ipb)

if isMicrochannel:
    os.system(name+" -t T_SC_OFF_M.txt ")
    os.system(name+" -t T_ANAG_ON_M.txt ")


print "########################################################################################## "

