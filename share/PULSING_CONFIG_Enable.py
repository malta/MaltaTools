#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import os
import sys
import time
import argparse

import MaltaBase
import MaltaSetup
from MaltaBase import *
import Herakles
import PyMaltaSlowControl

parser=argparse.ArgumentParser()
parser.add_argument('-R','--Row', help='Row to be acted upon',type=int,default=-1)
parser.add_argument('-C','--Col', help='Col to be acted upon',type=int,default=-1)
parser.add_argument('-D','--disable', help='Disable instead of enable',type=bool,default=False)
args=parser.parse_args()

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
connstr = MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
sc=PyMaltaSlowControl.MaltaSlowControl()
##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
os.system("MALTA_PSU.py -t T_SC_ON.txt ")
#time.sleep(0.1)

###################################
############# starting the SC clock
###################################
switchClockOn(ipb)

if args.disable:
    print "Disabling: "+str(args.Col)+" , "+str(args.Row)
    if args.Row!=-1: ipb.Write(5, sc.disablePulsePixelHor(args.Row))
    if args.Col!=-1: ipb.Write(5, sc.disablePulsePixelCol(args.Col))    
else:
    print "Enabling: "+str(args.Col)+" , "+str(args.Row)
    if args.Row!=-1: ipb.Write(5, sc.enablePulsePixelHor(args.Row))
    if args.Col!=-1: ipb.Write(5, sc.enablePulsePixelCol(args.Col))

DebugWord(ipb.Read(4), 0)

time.sleep(0.1)
switchClockOff(ipb)

os.system("MALTA_PSU.py -t T_SC_OFF.txt ")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
