#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time

import PyMaltaSlowControl
sc=PyMaltaSlowControl.MaltaSlowControl()

commands=[]

commands.append([sc.selfTest(),"Self Test"])
commands.append([sc.resetCMU(),"Reset CMU"])

commands.append([sc.switchDACMONI(False), "MonI False"])
commands.append([sc.switchDACMONI(True) , "MonI True"])
commands.append([sc.switchDACMONV(False), "MonV False"])
commands.append([sc.switchDACMONV(True) , "MonV True"])

commands.append([sc.switchVRESET_D(True)   , "SwitchON VRESET_D"]) 
commands.append([sc.switchVRESET_P(True)   , "SwitchON VRESET_P"]) 
commands.append([sc.switchVPLSE_LOW(True)  , "SwitchON VLOW"]) 
commands.append([sc.switchVPLSE_HIGH(True) , "SwitchON VHIGH"]) 
commands.append([sc.switchVCLIP(True)      , "SwitchON VCLIP"]) 
commands.append([sc.switchVCASN(True)      , "SwitchON VCASN"]) 

commands.append([sc.switchVRESET_D(False)  , "SwitchOFF VREST_D"]) 
commands.append([sc.switchVRESET_P(False)  , "SwitchOFF VREST_P"]) 
commands.append([sc.switchVPLSE_LOW(False) , "SwitchOFF VLOW"]) 
commands.append([sc.switchVPLSE_HIGH(False), "SwitchOFF VHIGH"]) 
commands.append([sc.switchVCLIP(False)     , "SwitchOFF VCLIP"]) 
commands.append([sc.switchVCASN(False)     , "SwitchOFF VCASN"]) 

for i in xrange(0,128):
    commands.append([sc.setVRESET_D(i)  , "Set VREST_D "+str(i)]) 
    commands.append([sc.setVRESET_P(i)  , "Set VREST_P "+str(i)]) 
    commands.append([sc.setVPLSE_LOW(i) , "Set VLOW "+str(i)]) 
    commands.append([sc.setVPLSE_HIGH(i), "Set VHIGH "+str(i)]) 
    commands.append([sc.setVCLIP(i)     , "Set VCLIP "+str(i)]) 
    commands.append([sc.setVCASN(i)     , "Set VCASN "+str(i)]) 

commands.append([sc.delay(500),"Del 500"])
commands.append([sc.delay(750),"Del 750"])
commands.append([sc.delay(1000),"Del 1000"])
commands.append([sc.delay(2000),"Del 2000"])

for i in xrange(0,32):
    commands.append([sc.readRegister(i),"Reading Register "+str(i)])

for i in xrange(0,256):
    commands.append([sc.maskDoubleColumn(i),"Masking DC "+str(i)])
    commands.append([sc.unmaskDoubleColumn(i),"Unmasking DC "+str(i)])

for i in xrange(0,512):
    commands.append([sc.enablePulsePixelCol(i) ,"Enable pulsing column "+str(i)])
    commands.append([sc.disablePulsePixelCol(i),"Disable pulsing column "+str(i)])

for i in xrange(0,512):
    commands.append([sc.enablePulsePixelHor(i) ,"Enable pulsing row "+str(i)])
    commands.append([sc.disablePulsePixelHor(i),"Disable pulsing row "+str(i)])


for c in commands:
    #print c
    wordHex=" 0x%04x"%(c[0]&0xFFFF)
    wordBin=" "+str( format( (c[0]&0xFFFF) , "016b" ) )
    print " "+wordBin+"  ,  "+wordHex+"  == "+c[1]
