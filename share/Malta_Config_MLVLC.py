#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse
from socket import gethostname

import MaltaBase
from MaltaBase import *
import Herakles
import PyMaltaSlowControl
from asyncore import read
from plistlib import readPlist


##################################################################################################
def getSCFifoState(ip):
    value=ip.Read(3)
    print " "
    print "FIFOread  full : "+str( ( (value >> 8) & 0x1)!=0 )
    print "FIFOread  empty: "+str( ( (value >> 7) & 0x1)==0 )
    print "FIFOwrite  full: "+str( ( (value >>10) & 0x1)!=0 )
    print "FIFOwrite empty: "+str( ( (value >> 9) & 0x1)==0 )


##################################################################################################
def getAllRegisters(ipb):
    res=[]
    for i in xrange(0,23):
        w=int(sc.readRegister(i))
        ipb.Write(5, w)
        val1=ipb.Read(4)
        res.append(val1&0xFFFF)
    return res


##################################################################################################
def doSelfTests(ipb, NTests):
    C_all    =0.
    C_correct=0.
    C_wrong  =0.
    DebugWord(ipb.Read(4),0)
    DebugWord(ipb.Read(4),0)
    DebugWord(ipb.Read(4),0)
    print " "
    print " "
    
    for i in xrange(0,NTests):
        C_all+=1.
        time.sleep(0.001)
        ipb.Write(5,0xA000)
        v1=DebugWord(ipb.Read(4),0)
        v2=DebugWord(ipb.Read(4),1)
        v3=DebugWord(ipb.Read(4),2)
        if v2==-1 and v1==43690: C_correct+=1.
        else                   : C_wrong  +=1.

    print " " 
    print "SUMMARY: sent tests: "+str(C_all)+"  ---> CORRECT results: "+str(C_correct)+" === "+str( C_correct/C_all*100)+" %"
    print "#########################################################################################################"
    print " "        


##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
#os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

hostname=gethostname()
##ipName="ADE-ID-ROUTER"
ipName="tcp://pcatlidrpi02:50001?target=192.168.0.1:50001"
#if "ros01" in hostname or "ros02" in hostname:
#    ipName="192.168.0.1"
if "ps03" in hostname:
    ipName="tcp://pcatlidrpi01:50001?target=192.168.0.1:50001"  
print "Trying to connect to: "+ipName
ipb=Herakles.Uhal(ipName)

GetVersion(ipb,True)
getSCFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)
##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure"
ipb.Write(5, sc.enableMerger(False))
ipb.Write(5, sc.delay(750))
DebugWord(ipb.Read(4), 0)

############# DAC monitoring
print "\n Disabling DAC monitoring"
ipb.Write(5, sc.switchDACMONI(False))
ipb.Write(5, sc.switchDACMONV(False))
#ipb.Write(5, sc.readRegister(21)    )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
############# DAC override
print "\n Switching on IDB"
ipb.Write(5, sc.switchIDB(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on ITHR"
ipb.Write(5, sc.switchITHR(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on IBIAS"
ipb.Write(5, sc.switchIBIAS(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on IRESET"
ipb.Write(5, sc.switchIRESET(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on ICASN"
ipb.Write(5, sc.switchICASN(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on VCASN"
ipb.Write(5, sc.switchVCASN(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on VCLIP"
ipb.Write(5, sc.switchVCLIP(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on VPLSE_HIGH"
ipb.Write(5, sc.switchVPLSE_HIGH(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on VPLSE_LOW"
ipb.Write(5, sc.switchVPLSE_LOW(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on VRESET_D"
ipb.Write(5, sc.switchVRESET_D(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)
print "\n Switching on VRESET_P"
ipb.Write(5, sc.switchVRESET_P(True) )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)

#############
#ipb.Write(5, sc.setIBUFP_MON1(8) )
#ipb.Write(5, sc.setIBUFN_MON1(15) )
#ipb.Write(5, sc.setIBUFP_MON0(8) )
#ipb.Write(5, sc.setIBUFN_MON0(15) )
ipb.Write(5, sc.readRegister(21)    )
DebugWord(ipb.Read(4), 0)
time.sleep(0.1)

print "\n Configuring masking and pulsing"

############# VD: disable everything that can produce noise in the chip
#ipb.Write(5, sc.disablePulsePixelHor(0))
#time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelCol(5))

switchClockOff(ipb)
time.sleep(0.5)
print "\n DONE "
