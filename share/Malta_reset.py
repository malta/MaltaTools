#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
#################################################################################################

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
#os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)

GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

##switchClockOn(ipb)

##sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
##print "\n Disabling the merger structure"
##ipb.Write(5, sc.enableMerger(False))
#ipb.Write(5, sc.enableMerger(True))
##ipb.Write(5, sc.delay(750))
#DebugWord(ipb.Read(4), 0)

############# DAC monitoring
#print "\n Disabling DAC monitoring"
##ipb.Write(5, sc.switchDACMONI(False))
#ipb.Write(5, sc.switchDACMONV(False))
##ipb.Write(5, sc.switchDACMONV(True))
#ipb.Write(5, sc.readRegister(21)    )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
############# DAC override
#print "\n Switching on IDB"
#ipb.Write(5, sc.switchIDB(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on ITHR"
#ipb.Write(5, sc.switchITHR(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on IBIAS"
#ipb.Write(5, sc.switchIBIAS(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on IRESET"
#ipb.Write(5, sc.switchIRESET(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on ICASN"
#ipb.Write(5, sc.switchICASN(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on VCASN"
#ipb.Write(5, sc.switchVCASN(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on VCLIP"
#ipb.Write(5, sc.switchVCLIP(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on VPLSE_HIGH"
#ipb.Write(5, sc.switchVPLSE_HIGH(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on VPLSE_LOW"
#ipb.Write(5, sc.switchVPLSE_LOW(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on VRESET_D"
#ipb.Write(5, sc.switchVRESET_D(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)
#print "\n Switching on VRESET_P"
#ipb.Write(5, sc.switchVRESET_P(True) )
#DebugWord(ipb.Read(4), 0)
#time.sleep(0.1)

#############
#ipb.Write(5, sc.setIBUFP_MON1(8) )
#ipb.Write(5, sc.setIBUFN_MON1(15) )
#ipb.Write(5, sc.setIBUFP_MON0(8) )
#ipb.Write(5, sc.setIBUFN_MON0(15) )
#ipb.Write(5, sc.readRegister(21)    )
#print "This is the readout register"
#DebugWord(ipb.Read(4), 0)
time.sleep(0.1)

#############LVDS stuff
#print "\n LVDS register before change:"
#ipb.Write(5,sc.readRegister(5))
#DebugWord(ipb.Read(4),0)
#time.sleep(0.1)
#ipb.Write(5,sc.configHBridge(31))
#DebugWord(ipb.Read(4),0)
#time.sleep(0.1)
#print "\n LVDS register after change:"
#ipb.Write(5,sc.readRegister(5))
#DebugWord(ipb.Read(4),0)
#time.sleep(0.1)

#print "\n Configuring masking and pulsing"

############# VD: disable everything that can produce noise in the chip
#ipb.Write(5, sc.disablePulsePixelHor(0))
#time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelCol(5))

#switchClockOff(ipb)
print "\n DONE "

