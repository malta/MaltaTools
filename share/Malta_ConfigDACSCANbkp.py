#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
args=parser.parse_args()
chip=args.chip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

readA =4
writeA=5
if args.dualchip:
    readA =90
    writeA=91


connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
print ("Done")
GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure"
ipb.Write(writeA, sc.enableMerger(False))
#ipb.Write(writeA, sc.enableMerger(True))
ipb.Write(writeA, sc.delay(750))
DebugWord(ipb.Read(readA), 0)

############# DAC monitoring
print "\n ENabling IDAC monitoring, DISABLING vdac MONITORING"
##ipb.Write(writeA, sc.switchDACMONI(False))
ipb.Write(writeA, sc.switchDACMONV(False))
##ipb.Write(writeA, sc.switchDACMONV(True))
ipb.Write(writeA, sc.switchDACMONI(True))
ipb.Write(writeA, sc.readRegister(21)    )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
############# DAC override
print "\n Switching on IDB"
ipb.Write(writeA, sc.switchIDB(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ITHR"
ipb.Write(writeA, sc.switchITHR(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IBIAS"
ipb.Write(writeA, sc.switchIBIAS(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IRESET"
ipb.Write(writeA, sc.switchIRESET(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ICASN"
ipb.Write(writeA, sc.switchICASN(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCASN"
ipb.Write(writeA, sc.switchVCASN(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCLIP"
ipb.Write(writeA, sc.switchVCLIP(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_HIGH"
ipb.Write(writeA, sc.switchVPLSE_HIGH(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_LOW"
ipb.Write(writeA, sc.switchVPLSE_LOW(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_D"
ipb.Write(writeA, sc.switchVRESET_D(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_P"
ipb.Write(writeA, sc.switchVRESET_P(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

#############
#ipb.Write(writeA, sc.setIBUFP_MON1(8) )
#ipb.Write(writeA, sc.setIBUFN_MON1(15) )
#ipb.Write(writeA, sc.setIBUFP_MON0(8) )
#ipb.Write(writeA, sc.setIBUFN_MON0(15) )
ipb.Write(writeA, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

#############LVDS stuff
#print "\n LVDS register before change:"
#ipb.Write(writeA,sc.readRegister(5))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)
#ipb.Write(writeA,sc.configHBridge(31))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)
#print "\n LVDS register after change:"
#ipb.Write(writeA,sc.readRegister(5))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)

print "\n Configuring masking and pulsing"

############# VD: disable everything that can produce noise in the chip
#ipb.Write(writeA, sc.disablePulsePixelHor(0))
#time.sleep(0.1)
#ipb.Write(writeA, sc.disablePulsePixelCol(5))

switchClockOff(ipb)
print "\n DONE "

