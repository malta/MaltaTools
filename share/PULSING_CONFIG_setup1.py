#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import os
import sys
import time
import argparse

import MaltaBase
import MaltaSetup
from MaltaBase import *
import Herakles
import PyMaltaSlowControl

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip', help='Which chip to talk to'  ,type=int,default=1)
args=parser.parse_args()
chip=args.chip

connstr = MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)

GetVersion(ipb,True)
print GetStatus(ipb)
getFifoState(ipb)
ResetFifo(ipb)
##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ") ##was commented out
os.system("MALTA_PSU.py -t T_SC_ON.txt ")    ##was commented out
time.sleep(0.1)

###################################
############# starting the SC clock
###################################
switchClockOn(ipb)

############# MaltaSlowControl instance
sc=PyMaltaSlowControl.MaltaSlowControl()

time.sleep(0.1)
##############################################ipb.Write(5, sc.enablePulsePixelHor(1))
##ipb.Write(5, sc.enablePulsePixelHor(6))
#ipb.Write(5, sc.enablePulsePixelHor(1))
##ipb.Write(5, sc.enablePulsePixelHor(1))
####ipb.Write(5, sc.enablePulsePixelHor(2))
####ipb.Write(5, sc.enablePulsePixelCol(0))
#ipb.Write(5, sc.enablePulsePixelCol(1))
##ipb.Write(5, sc.enablePulsePixelCol(4))
####ipb.Write(5, sc.enablePulsePixelCol(2))
##############################################ipb.Write(5, sc.enablePulsePixelCol(0))
#ipb.Write(5, sc.enablePulsePixelCol(100))
#ipb.Write(5, sc.enablePulsePixelCol(70))

time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(251))
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(251))
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(251))
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(251))
time.sleep(0.1)
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(511))
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(511))
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(511))
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(511))
time.sleep(0.1)
time.sleep(0.1)
#ipb.Write(5, sc.disablePulsePixelHor(0))
time.sleep(0.1)

#############################################################################
ipb.Write(5, sc.enablePulsePixelCol(1))
ipb.Write(5, sc.enablePulsePixelCol(1))
ipb.Write(5, sc.enablePulsePixelHor(1))
ipb.Write(5, sc.enablePulsePixelHor(1))
##ipb.Write(5, sc.enablePulsePixelCol(127))

#for i in xrange(64,127):
#    ipb.Write(5, sc.enablePulsePixelCol(i))
#    time.sleep(0.1)

#ipb.Write(5, sc.enablePulsePixelCol(62)) 
#ipb.Write(5, sc.enablePulsePixelCol(93)) 

#for i_row in range(0,256):
#    ipb.Write(5, sc.enablePulsePixelCol(i_row) )
#    time.sleep(0.01)

#for i_row in range(0,10):
#    ipb.Write(5, sc.enablePulsePixelCol(i_row) )
#    time.sleep(0.01)
DebugWord(ipb.Read(4), 0)

#ipb.Write(5, sc.enablePulsePixelCol(181) )
#time.sleep(0.1)

##ipb.Write(5,  sc.maskDoubleColumn(111) )

############# stopping the SC clock
time.sleep(1.0)
switchClockOff(ipb)
print "End of script.\n"

os.system("MALTA_PSU.py -t T_SC_OFF.txt ")   ##was commented out
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")  ##was commented out
