#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse
from socket import gethostname

import MaltaSetup
import MaltaBase
from MaltaBase import *
import Herakles
import PyMaltaSlowControl
from asyncore import read
from plistlib import readPlist

readA =4
writeA=5
readB=90
writeB=91

def printVal(value):
    val=""
    val+=" "+str( format( (value&0xFFFF) , "016b" ) )
    val+=" ::::   0x%04x"%(value&0xFFFF)
    return val

##################################################################################################
def DebugWord(word, attempt,ref=0):
    #time.sleep(0.001)
    #val="Attempt "+str(attempt) 
    val="Register: "+str( attempt)
    if word!=0: 
        val+="  NOT EMPTY "
        val+=" ::::  "+str( format( (word&0xFFFF) , "016b" ) )
        val+=" ::::   0x%04x"%(word&0xFFFF)
        val+=" ::::   %i"%(word&0xFFFF)
        if ref!=0:
            if ref!=(word&0xFFFF): val+="   <=========== "
    else      : val+="    EMPTY   "
    print val
    if word!=0: 
        return (word&0xFFFF)
    else:  
        return -1

##################################################################################################
def Read(val):
    ###val=h.Read(8)
    tmpList=list('{0:0b}'.format(val))
    print tmpList

##################################################################################################
def getFifoState(ip):
    value=ip.Read(3)
    print " "
    print "FIFOread  full : "+str( ( (value >> 8) & 0x1)!=0 )
    print "FIFOread  empty: "+str( ( (value >> 7) & 0x1)==0 )
    print "FIFOwrite  full: "+str( ( (value >>10) & 0x1)!=0 )
    print "FIFOwrite empty: "+str( ( (value >> 9) & 0x1)==0 )

#########################################################################################
def CMD_ResetMalta(herIP):
    ref = 0xFFFFFFFF
    bitIndex=12
    mask=ref ^ (1<<bitIndex)
    cacheWord= herIP.Read(8)
    herIP.Write(8, cacheWord | (1<<12) )
    time.sleep(1.0)
    herIP.Write(8, cacheWord & mask )
    print "DONE RESETTING MALTA"
    return 


##################################################################################################
def getAllRegisters(ipb):
    res=[]
    #for i in xrange(0,23):
    for i in xrange(0,32):
        time.sleep(0.02)
        w=int(sc.readRegister(i))#diccionario de comandos
        ###print printVal(w)
        ipb.Write(writeA, w)
        val1=ipb.Read(readA)
        res.append(val1&0xFFFF)
    return res

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipName=connstr
ipb=Herakles.Uhal(ipName)
GetVersion(ipb,True)
print GetStatus(ipb)

ResetFifo(ipb)
getFifoState(ipb)

parser = argparse.ArgumentParser()
parser.add_argument("-V","--verbose",help="verbose mode",action='store_true')
parser.add_argument("--n",help="Reading N a register", required=True)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-bc' ,'--bothchips', help='sending test to both chips'  ,action='store_true')
args=parser.parse_args()

if args.dualchip:
    readA =90
    writeA=91
    readB=90
    write=91

if args.bothchips:
    readB =90
    writeB=91
    readA =4
    writeA=5


##sys.exit()

switchClockOn(ipb)

C_all    =0.
C_correct=0.
C_wrong  =0.
##########################################ipb.Write(writeA,0x6000) #disable merger
### writing what we think is the default configuration

DebugWord(ipb.Read(readA),0)
DebugWord(ipb.Read(readA),0)
DebugWord(ipb.Read(readA),0)
print " "
print " "

### a bunch of self test
for i in xrange(0,1000): #####
    C_all+=1.
    print "chipID0"
    print " "+str(i)
    time.sleep(0.05)
    ipb.Write(writeA,0xA000)
    #time.sleep(1)
    #ipb.Write(writeA,0x8010)
    ##ipb.Write(writeA,0x8150)
    v1=DebugWord(ipb.Read(readA),0)
    #time.sleep(1)
    v2=DebugWord(ipb.Read(readA),1)
    if v2==-1 and v1==43690: C_correct+=1.
    ###if v2==-1 and v1==2828 : C_correct+=1.
    else                   : C_wrong  +=1.
    C_all+=1.
    print "chipID1"
    print " "+str(i)
    time.sleep(0.05)
    ipb.Write(writeB,0xA000)
    #time.sleep(1)
    #ipb.Write(writeA,0x8010)
    ##ipb.Write(writeA,0x8150)
    v1=DebugWord(ipb.Read(readB),0)
    #time.sleep(1)
    v2=DebugWord(ipb.Read(readB),1)
    if v2==-1 and v1==43690: C_correct+=1.
    ###if v2==-1 and v1==2828 : C_correct+=1.
    else                   : C_wrong  +=1.

print " " 
print " " 
print "SUMMARY: "
print "  -- sent tests: "+str(C_all)+"  ---> CORRECT results: "+str(C_correct)+" === "+str( C_correct/C_all*100)+" %"
print "#########################################################################################################"
print " "

print " "
print "End of script.\n"














