#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-DVDD' ,'--DVDD'       , help='DVDD when setting DAC, it will go to 1.9 when reading the DAC',type=float,default="0.8")
args=parser.parse_args()
chip=args.chip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(args.DVDD)+ " ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

readA =4
writeA=5
if args.dualchip:
    readA =90
    writeA=91


connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
#if chip==0:
#    ipb=Herakles.Uhal("udp://ep-ade-gw-01.cern.ch:50006")
#else:
#    ipb=Herakles.Uhal(connstr)
print ("Done")
GetVersion(ipb,True)
getFifoState(ipb)

  

ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n DATA Flow CHIP 0(Right on the board)\n"
ipb.Write(5, sc.readRegister(1))
DebugWord(ipb.Read(4), 0)
DebugWord(ipb.Read(4), 0)
#Register: 0  NOT EMPTY  ::::  0000011110101000 ::::   0x07a8 ::::   1960

print "\n DATA Flow CHIP 1(Left on the board)\n"
ipb.Write(91, sc.readRegister(1))
DebugWord(ipb.Read(90), 0)
DebugWord(ipb.Read(90), 0)
#Register: 0  NOT EMPTY  ::::  0000010001010010 ::::   0x0452 ::::   1106

switchClockOff(ipb)


print "\n DONE "
os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
