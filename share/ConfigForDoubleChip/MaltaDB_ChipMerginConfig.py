#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-DVDD' ,'--DVDD'       , help='DVDD when setting DAC, it will go to 1.9 when reading the DAC',type=float,default="0.8")
parser.add_argument('-r' ,'--rst'    , help='Reset the chip befor configuring',action='store_true')
parser.add_argument('-o' ,'--out'    , help='Set the output chip (0 or 1) default 0',type=int, default=0)
parser.add_argument('-s' ,'--secondary'    , help='Set the secondary source, if not desired enter -s== -o',type=int, default=1)
args=parser.parse_args()
chip=args.chip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(args.DVDD)+ " ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

readA=4
writeA=5
read0=4
write0=5
read1=90
write1=91
if args.dualchip:
    readA =90
    writeA=91    


connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
#if chip==0:
#    ipb=Herakles.Uhal("udp://ep-ade-gw-01.cern.ch:50006")
#else:
#    ipb=Herakles.Uhal(connstr)
print ("Done")
GetVersion(ipb,True)
getFifoState(ipb)
if args.rst:
  print "***Reseting both chips*** "
  ResetMalta(ipb)
  

ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure for both chips"
ipb.Write(5, sc.enableMerger(False))
ipb.Write(91, sc.enableMerger(False))

ipb.Write(5, sc.delay(750)) #was 750
ipb.Write(91, sc.delay(750)) #was 750
DebugWord(ipb.Read(readA), 0)

if (args.out==0 and args.secondary==1):
  ###
  print "\n Chip 0 is the output. Chip 1 sending data to Chip 0. Use firmware v2 and DoubleChipPlotting_chip0."  
  ###
  print "\n Configuring CHIP 0 to receive data from the left"
  ipb.Write(write0,sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write0,sc.enableCMOSLeft(False)) #Deafaul 1
  ipb.Write(write0,sc.enableDataflowLVDS(True)) #Default 1
  ipb.Write(write0,sc.enableRightCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableLeftCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  ipb.Write(write0,sc.enableRightMergerToLVDS(True))#Send data of Rmerger to LVDS Def 0 Must be there
  ipb.Write(write0,sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableLeftMergerToLVDS(False))#Send of Lmerger to LVDS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableMergerToRight(True))#Send data to the right Def 0 #### If swapped see just chip 1
  ipb.Write(write0,sc.enableMergerToLeft(False))#Send data to the left Def 1#### If swapped see just chip 1
  print "\n Reading Register DATA FLOW==>MSB 010010001000 LSB"
  ipb.Write(write0, sc.readRegister(1)) 
  DebugWord(ipb.Read(read0), 0)
  DebugWord(ipb.Read(read0), 0)
  ###

  print "\n Configuring CHIP 1 to send data to the right"
  ipb.Write(write1, sc.enableMergerToLeft(False))#Send data to the left Def 1
  ipb.Write(write1, sc.enableMergerToRight(True))#Send data to the right Def 0

  ipb.Write(write1, sc.enableLeftMergerToLVDS(False))#Send data of Lmerger to LVDS Def1
  ipb.Write(write1, sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1
  ipb.Write(write1, sc.enableRightMergerToLVDS(False))#Send data of Rmerger to LVDS Def 0
  ipb.Write(write1, sc.enableRightMergerToCMOS(True))#send data of Rmerger to CMOS Def 0
  #Enable propagation to top pads
  ipb.Write(write1, sc.enableLeftCMOSTop(False)) #Default 0
  ipb.Write(write1, sc.enableRightCMOSTop(True))#Default 0
  #Enable the LVDS driver
  ipb.Write(write1, sc.enableDataflowLVDS(False)) #Default 1
  #Enable the driver CMOS
  ipb.Write(write1, sc.enableCMOSRight(True)) #Default 0
  ipb.Write(write1, sc.enableCMOSLeft(False)) #Deafaul 1

  print "\n Reading Register DATA FLOW==>MSB 010001010010 LSB"
  ipb.Write(write1, sc.readRegister(1))
  DebugWord(ipb.Read(read1), 0)
  DebugWord(ipb.Read(read1), 0)
###


if (args.out==0 and args.secondary==0):
  ###
  print "\n Chip 0 is the output. No data from Chip1. Use firmware v2 and DoubleChipPlotting_chip0."  
  ###
  print "\n Configuring CHIP 0 to receive data from the left. NB enabled CMOS Input Top Left"
  ipb.Write(write0,sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write0,sc.enableCMOSLeft(False)) #Deafaul 1
  ipb.Write(write0,sc.enableDataflowLVDS(True)) #Default 1
  ipb.Write(write0,sc.enableRightCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableLeftCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  ipb.Write(write0,sc.enableRightMergerToLVDS(True))#Send data of Rmerger to LVDS Def 0 Must be there
  ipb.Write(write0,sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableLeftMergerToLVDS(False))#Send of Lmerger to LVDS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableMergerToRight(True))#Send data to the right Def 0 #### If swapped see just chip 1
  ipb.Write(write0,sc.enableMergerToLeft(False))#Send data to the left Def 1#### If swapped see just chip 1
  print "\n Reading Register DATA FLOW==>MSB 010010001000 LSB"
  ipb.Write(write0, sc.readRegister(1))    
  DebugWord(ipb.Read(read0), 0)
  DebugWord(ipb.Read(read0), 0)
  ###

  print "\n Configuring CHIP 1 to NOT send data."
  ipb.Write(write1, sc.enableMergerToLeft(False))#Send data to the left Def 1
  ipb.Write(write1, sc.enableMergerToRight(False))#Send data to the right Def 0

  ipb.Write(write1, sc.enableLeftMergerToLVDS(False))#Send data of Lmerger to LVDS Def1
  ipb.Write(write1, sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1
  ipb.Write(write1, sc.enableRightMergerToLVDS(False))#Send data of Rmerger to LVDS Def 0
  ipb.Write(write1, sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  #Enable propagation to top pads
  ipb.Write(write1, sc.enableLeftCMOSTop(False)) #Default 0
  ipb.Write(write1, sc.enableRightCMOSTop(False))#Default 0
  #Enable the LVDS driver
  ipb.Write(write1, sc.enableDataflowLVDS(False)) #Default 1
  #Enable the driver CMOS
  ipb.Write(write1, sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write1, sc.enableCMOSLeft(False)) #Deafaul 1

  print "\n Reading Register DATA FLOW==>MSB 000000000000 LSB"
  ipb.Write(write1, sc.readRegister(1))
  DebugWord(ipb.Read(read1), 0)
  DebugWord(ipb.Read(read1), 0)
###

if (args.out==1 and args.secondary==0):
  ###
  print "\n Chip 1 is the output. Chip 0 sending data to Chip 1. Use firmware v2_chip2 and DoubleChipPlotting_chip0."  
  ###_chip
  print "\n Configuring CHIP 1 to receive data from the right"
  ipb.Write(write1,sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write1,sc.enableCMOSLeft(False)) #Deafaul 1
  ipb.Write(write1,sc.enableDataflowLVDS(True)) #Default 1
  ipb.Write(write1,sc.enableRightCMOSTop(False))#Default 0
  ipb.Write(write1,sc.enableLeftCMOSTop(False))#Default 0
  ipb.Write(write1,sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  ipb.Write(write1,sc.enableRightMergerToLVDS(False))#Send data of Rmerger to LVDS Def 0 Must be there
  ipb.Write(write1,sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1####it was true Not Necessary
  ipb.Write(write1,sc.enableLeftMergerToLVDS(True))#Send of Lmerger to LVDS Def1####it was true Not Necessary
  ipb.Write(write1,sc.enableMergerToRight(False))#Send data to the right Def 0 #### If swapped see just chip 1
  ipb.Write(write1,sc.enableMergerToLeft(True))#Send data to the left Def 1#### If swapped see just chip 1
  print "\n Reading Register DATA FLOW==>MSB 101000001000 LSB"
  ipb.Write(write1, sc.readRegister(1)) 
  DebugWord(ipb.Read(read1), 0)
  DebugWord(ipb.Read(read1), 0)
  ###

  print "\n Configuring CHIP 0 to send data to the left"
  ipb.Write(write0,sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write0,sc.enableCMOSLeft(True)) #Deafaul 1
  ipb.Write(write0,sc.enableDataflowLVDS(False)) #Default 1
  ipb.Write(write0,sc.enableRightCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableLeftCMOSTop(True))#Default 0
  ipb.Write(write0,sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  ipb.Write(write0,sc.enableRightMergerToLVDS(False))#Send data of Rmerger to LVDS Def 0 Must be there
  ipb.Write(write0,sc.enableLeftMergerToCMOS(True))#send data of Lmerger to CMOS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableLeftMergerToLVDS(False))#Send of Lmerger to LVDS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableMergerToRight(False))#Send data to the right Def 0 #### If swapped see just chip 1
  ipb.Write(write0,sc.enableMergerToLeft(True))#Send data to the left Def 1#### If swapped see just chip 1

  print "\n Reading Register DATA FLOW==>MSB 100100100100 LSB"
  ipb.Write(write0, sc.readRegister(1))
  DebugWord(ipb.Read(read0), 0)
  DebugWord(ipb.Read(read0), 0)
###



if (args.out==1 and args.secondary==1):
  ###
  print "\n Chip 1 is the output. No data from Chip0. Use firmware v2 and DoubleChipPlotting_chip0"  
  ###_chip
  print "\n Configuring CHIP 1 to receive data from the right. NB enabled CMOS Input Top Right"
  ipb.Write(write1,sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write1,sc.enableCMOSLeft(False)) #Deafaul 1
  ipb.Write(write1,sc.enableDataflowLVDS(True)) #Default 1
  ipb.Write(write1,sc.enableRightCMOSTop(False))#Default 0
  ipb.Write(write1,sc.enableLeftCMOSTop(False))#Default 0
  ipb.Write(write1,sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  ipb.Write(write1,sc.enableRightMergerToLVDS(False))#Send data of Rmerger to LVDS Def 0 Must be there
  ipb.Write(write1,sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1####it was true Not Necessary
  ipb.Write(write1,sc.enableLeftMergerToLVDS(True))#Send of Lmerger to LVDS Def1####it was true Not Necessary
  ipb.Write(write1,sc.enableMergerToRight(False))#Send data to the right Def 0 #### If swapped see just chip 1
  ipb.Write(write1,sc.enableMergerToLeft(True))#Send data to the left Def 1#### If swapped see just chip 1
  print "\n Reading Register DATA FLOW==>MSB 101000001000 LSB"
  ipb.Write(write1, sc.readRegister(1)) 
  DebugWord(ipb.Read(read1), 0)
  DebugWord(ipb.Read(read1), 0)
  ###

  print "\n Configuring CHIP 0 to send data to the left"
  ipb.Write(write0,sc.enableCMOSRight(False)) #Default 0
  ipb.Write(write0,sc.enableCMOSLeft(False)) #Deafaul 1
  ipb.Write(write0,sc.enableDataflowLVDS(False)) #Default 1
  ipb.Write(write0,sc.enableRightCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableLeftCMOSTop(False))#Default 0
  ipb.Write(write0,sc.enableRightMergerToCMOS(False))#send data of Rmerger to CMOS Def 0
  ipb.Write(write0,sc.enableRightMergerToLVDS(False))#Send data of Rmerger to LVDS Def 0 Must be there
  ipb.Write(write0,sc.enableLeftMergerToCMOS(False))#send data of Lmerger to CMOS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableLeftMergerToLVDS(False))#Send of Lmerger to LVDS Def1####it was true Not Necessary
  ipb.Write(write0,sc.enableMergerToRight(False))#Send data to the right Def 0 #### If swapped see just chip 1
  ipb.Write(write0,sc.enableMergerToLeft(False))#Send data to the left Def 1#### If swapped see just chip 1

  print "\n Reading Register DATA FLOW==>MSB 000000000000 LSB"
  ipb.Write(write0, sc.readRegister(1))
  DebugWord(ipb.Read(read0), 0)
  DebugWord(ipb.Read(read0), 0)
###


switchClockOff(ipb)
print "\n DONE "
os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
