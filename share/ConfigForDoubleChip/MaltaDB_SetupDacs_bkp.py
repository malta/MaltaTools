#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions


######################
##
##author Roberto.Cardella@cern.ch
##
######################
#######################
#
# This script configure the double chip malta (carrier v2) DACs
# Use -r if you want to reset at the begining of the script
# MALTA C use -DVDD 0.8 to configure (default)
# To configure data transmission from chip2chip use another script, i.e. MaltaDB_ChipMerginConfig.py
#
##############################################
##############################################

import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-DVDD' ,'--DVDD'       , help='DVDD when setting DAC, it will go to 1.9 when reading the DAC',type=float,default="0.8")
parser.add_argument('-r' ,'--rst'    , help='Reset the chip befor configuring',action='store_true')
parser.add_argument('-m' ,'--mask'    , help='Enable Masking of evaluated pixel',action='store_true')
args=parser.parse_args()
chip=args.chip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(args.DVDD)+ " ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")


readA=4
writeA=5
read0=4
write0=5
read1=90
write1=91
if args.dualchip:
    readA =90
    writeA=91    


connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
#if chip==0:
#    ipb=Herakles.Uhal("udp://ep-ade-gw-01.cern.ch:50006")
#else:
#    ipb=Herakles.Uhal(connstr)
print ("Done")
GetVersion(ipb,True)
getFifoState(ipb)
if args.rst:
  print "***Reseting both chips*** "
  ResetMalta(ipb)
  

ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure for both chips"
ipb.Write(5, sc.enableMerger(False))
ipb.Write(91, sc.enableMerger(False))

ipb.Write(5, sc.delay(750)) #was 750
ipb.Write(91, sc.delay(750)) #was 750
DebugWord(ipb.Read(read0), 0)

print "\n **********************"
print "\n ******CHIP 0**********"
print "\n **********************"
############# DAC monitoring
print "\n Disabling DAC monitoring"
ipb.Write(write0, sc.switchDACMONV(False))
ipb.Write(write0, sc.switchDACMONI(False))
ipb.Write(write0, sc.readRegister(21)    )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
############# DAC override
print "\n Switching on IDB"
ipb.Write(write0, sc.switchIDB(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on ITHR"
ipb.Write(write0, sc.switchITHR(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on IBIAS"
ipb.Write(write0, sc.switchIBIAS(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on IRESET"
ipb.Write(write0, sc.switchIRESET(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on ICASN"
ipb.Write(write0, sc.switchICASN(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VCASN"
ipb.Write(write0, sc.switchVCASN(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VCLIP"
ipb.Write(write0, sc.switchVCLIP(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_HIGH"
ipb.Write(write0, sc.switchVPLSE_HIGH(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_LOW"
ipb.Write(write0, sc.switchVPLSE_LOW(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VRESET_D"
ipb.Write(write0, sc.switchVRESET_D(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VRESET_P"
ipb.Write(write0, sc.switchVRESET_P(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)


##### SETTING miniMALTA values in MALTA ##########
print ("\n Setting ICASN ")
ipb.Write(write0, sc.setICASN(2)) 
ipb.Write(write0, sc.setICASN(2)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting IBIAS ")
ipb.Write(write0, sc.setIBIAS(100)) #50 miniMALTA
ipb.Write(write0, sc.setIBIAS(100)) #50 miniMALTA
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting ITHR")
ipb.Write(write0, sc.setITHR(20)) #WAS 1 
ipb.Write(write0, sc.setITHR(20)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting IDB")
ipb.Write(write0, sc.setIDB(50)) 
ipb.Write(write0, sc.setIDB(50)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting IRESET")
ipb.Write(write0, sc.setIRESET(80)) 
ipb.Write(write0, sc.setIRESET(80)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VCASN")
ipb.Write(write0, sc.setVCASN(64)) 
ipb.Write(write0, sc.setVCASN(64)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VRESET_P")
ipb.Write(write0, sc.setVRESET_P(60)) 
ipb.Write(write0, sc.setVRESET_P(60)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VRESET_D")
ipb.Write(write0, sc.setVRESET_D(58)) 
ipb.Write(write0, sc.setVRESET_D(58)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_HIGH")
ipb.Write(write0, sc.setVPLSE_HIGH(90)) 
ipb.Write(write0, sc.setVPLSE_HIGH(90)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_LOW")
ipb.Write(write0, sc.setVPLSE_LOW(5)) 
ipb.Write(write0, sc.setVPLSE_LOW(5)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VCLIP")
ipb.Write(write0, sc.setVCLIP(127)) 
ipb.Write(write0, sc.setVCLIP(127)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

#############
#ipb.Write(write0, sc.setIBUFP_MON1(8) )
#ipb.Write(write0, sc.setIBUFN_MON1(15) )
#ipb.Write(write0, sc.setIBUFP_MON0(8) )
#ipb.Write(write0, sc.setIBUFN_MON0(15) )
ipb.Write(write0, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print "\n **********************"
print "\n ******CHIP 1**********"
print "\n **********************"
############# DAC monitoring
print "\n Disabling DAC monitoring"
ipb.Write(write1, sc.switchDACMONV(False))
ipb.Write(write1, sc.switchDACMONI(False))
ipb.Write(write1, sc.readRegister(21)    )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
############# DAC override
print "\n Switching on IDB"
ipb.Write(write1, sc.switchIDB(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on ITHR"
ipb.Write(write1, sc.switchITHR(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on IBIAS"
ipb.Write(write1, sc.switchIBIAS(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on IRESET"
ipb.Write(write1, sc.switchIRESET(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on ICASN"
ipb.Write(write1, sc.switchICASN(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VCASN"
ipb.Write(write1, sc.switchVCASN(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VCLIP"
ipb.Write(write1, sc.switchVCLIP(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_HIGH"
ipb.Write(write1, sc.switchVPLSE_HIGH(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_LOW"
ipb.Write(write1, sc.switchVPLSE_LOW(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VRESET_D"
ipb.Write(write1, sc.switchVRESET_D(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)
print "\n Switching on VRESET_P"
ipb.Write(write1, sc.switchVRESET_P(False) )
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)


##### SETTING miniMALTA values in MALTA ##########
print ("\n Setting ICASN ")
ipb.Write(write1, sc.setICASN(2)) 
ipb.Write(write1, sc.setICASN(2)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting IBIAS ")
ipb.Write(write1, sc.setIBIAS(100)) #50 miniMALTA
ipb.Write(write1, sc.setIBIAS(100)) #50 miniMALTA
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting ITHR")
ipb.Write(write1, sc.setITHR(20)) #WAS 1 
ipb.Write(write1, sc.setITHR(20)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting IDB")
ipb.Write(write1, sc.setIDB(50)) 
ipb.Write(write1, sc.setIDB(50)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting IRESET")
ipb.Write(write1, sc.setIRESET(80)) 
ipb.Write(write1, sc.setIRESET(80)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VCASN")
ipb.Write(write1, sc.setVCASN(64)) 
ipb.Write(write1, sc.setVCASN(64)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VRESET_P")
ipb.Write(write1, sc.setVRESET_P(70)) 
ipb.Write(write1, sc.setVRESET_P(70)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VRESET_D")
ipb.Write(write1, sc.setVRESET_D(58)) 
ipb.Write(write1, sc.setVRESET_D(58)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_HIGH")
ipb.Write(write1, sc.setVPLSE_HIGH(90)) 
ipb.Write(write1, sc.setVPLSE_HIGH(90)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_LOW")
ipb.Write(write1, sc.setVPLSE_LOW(5)) 
ipb.Write(write1, sc.setVPLSE_LOW(5)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

print ("\n Setting VCLIP")
ipb.Write(write1, sc.setVCLIP(127)) 
ipb.Write(write1, sc.setVCLIP(127)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)

#############
#ipb.Write(write1, sc.setIBUFP_MON1(8) )
#ipb.Write(write1, sc.setIBUFN_MON1(15) )
#ipb.Write(write1, sc.setIBUFP_MON0(8) )
#ipb.Write(write1, sc.setIBUFN_MON0(15) )
ipb.Write(write1, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(read0), 0)
time.sleep(0.05)


print "\n **********************"
print "\n ******END OF DACS SETTING**********"
print "\n **********************"





print "\n Configuring masking and pulsing"

############# VD: disable everything that can produce noise in the chip

'''
for i in range(0,512):
    ipb.Write( 5,sc.maskPixelCol(i) )
    ipb.Write( 5,sc.maskPixelCol(i) )
    ipb.Write( 5,sc.maskPixelDiag(i) )
    ipb.Write( 5,sc.maskPixelDiag(i) )
    time.sleep(0.001)

dcStart   =100
dcLength=  16
'''
if args.mask:
  print "\n Masking Chip0 W7R23"
  ipb.Write(write0,sc.maskDoubleColumn(53)   )
  ipb.Write(write0,sc.maskDoubleColumn(60)   )
  ipb.Write(write0,sc.maskDoubleColumn(180) )
  ipb.Write(write0,sc.maskDoubleColumn(184) )
  time.sleep(0.001)
  ipb.Write(write0,sc.maskDoubleColumn(53)   )
  ipb.Write(write0,sc.maskDoubleColumn(60)   )
  ipb.Write(write0,sc.maskDoubleColumn(180) )
  ipb.Write(write0,sc.maskDoubleColumn(184) )
  print "\n"
  print "\n Masking Chip1 W7R22"
  time.sleep(0.001)
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(35) )
  ipb.Write(write1,sc.maskDoubleColumn(35) )
  ipb.Write(write1,sc.maskDoubleColumn(53) )
  ipb.Write(write1,sc.maskDoubleColumn(53) )
  ipb.Write(write1,sc.maskDoubleColumn(55) )
  ipb.Write(write1,sc.maskDoubleColumn(55) )
  ipb.Write(write1,sc.maskDoubleColumn(183) )
  ipb.Write(write1,sc.maskDoubleColumn(183) )
  time.sleep(0.001)
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(35) )
  ipb.Write(write1,sc.maskDoubleColumn(53) )
  time.sleep(0.001)
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(32) )
  ipb.Write(write1,sc.maskDoubleColumn(55) )
  ipb.Write(write1,sc.maskDoubleColumn(183) )
time.sleep(0.001)
print "\n Configuring masking and pulsing"

'''
for i in range(dcStart+dcLength,256):
    ipb.Write( write0,sc.maskDoubleColumn(i) )
    ipb.Write( write0,sc.maskDoubleColumn(i) )
    time.sleep(0.001)
DebugWord(ipb.Read(read0), 0)
DebugWord(ipb.Read(read0), 0)

rowStart=350
rowLength=130

for i in range(0,rowStart):
    ipb.Write( write0,sc.maskPixelHor(i) )
    ipb.Write( write0,sc.maskPixelHor(i) )
    time.sleep(0.001)

for i in range(rowStart+rowLength,512):
    ipb.Write( write0,sc.maskPixelHor(i) )
    ipb.Write( write0,sc.maskPixelHor(i) )
    time.sleep(0.001)

# For chip W7R21 very noisy in column 255
"""
ipb.Write( write0,sc.maskDoubleColumn(128))
ipb.Write( write0,sc.maskDoubleColumn(128))
time.sleep(0.001)
ipb.Write( write0,sc.maskDoubleColumn(127))
ipb.Write( write0,sc.maskDoubleColumn(127))
time.sleep(0.001)
ipb.Write( write0,sc.maskDoubleColumn(255))
ipb.Write( write0,sc.maskDoubleColumn(255))
time.sleep(0.001)
"""
DebugWord(ipb.Read(read0), 0)
DebugWord(ipb.Read(read0), 0)
'''

switchClockOff(ipb)
print "\n DONE "
os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
