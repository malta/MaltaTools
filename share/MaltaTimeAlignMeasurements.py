
#!/usr/bin/env python
####################################
## Daq script for MALTA read-out
# Dummy sender configuration values:
#
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# ignacio.asensi@cern.ch
# December 2017
####################################

import Herakles
import datetime
import argparse
import array
import time
import sys
import ROOT
import MaltaData  #output parsing  
import random
import os
import MaltaBase #configuration
import signal
from MaltaBase import *


parser=argparse.ArgumentParser()
parser.add_argument('-c','--conn_str',help='connection string',default="192.168.0.1")
parser.add_argument('-rf','--rootfile',help='ROOT output file. Not available yet',default="output.root")
parser.add_argument('-v','--verbose',help='Verbose information',action='store_true')
parser.add_argument('-n','--nevts',help='Number of tests',type=int, required=True)
parser.add_argument('-r','--report',help='Report output file. Default is output/MaltaTimeAlignMeasurements.log',default="output/MaltaRODummySender_ignacio.log")
parser.add_argument('-te','--terminal',help='Display output on terminal. Besides from output file.',action='store_true')
parser.add_argument('-ch0','--ch0',help='Print only channel 0. Good for large tests',action='store_true')
parser.add_argument('-root','--root',help='Display root histogram of pulses width',action='store_true')
 

args=parser.parse_args()
verbose=False
verbose=args.verbose

terminal=False
terminal=args.terminal

verbose=args.verbose
nevts=args.nevts

root = False
root=args.root


time.ctime() # 'Mon Oct 18 13:35:29 2010'
time_str=time.strftime('%Y%m%d_%H%M%S')



logname="output/MaltaTimeAlignMeasurements_%s.log" % time_str
rootname="output/MaltaTimeAlignMeasurements_%s.root" % time_str



#rootfile
rf=ROOT.TFile(rootname,"new")


print "Start time: %s" % time.strftime('%H:%M:%S %b %d %Y\n')

#Connection

ipb=Herakles.Uhal(args.conn_str)

md = MaltaData.MaltaData()





def handler(signum, frame):
    report_str=""
    report_str+= "You pressed ctr+c to quit\n"
    m_cont=False
    pass
def verboseData(self, data):
    report_str=""
    report_str+= "refbit: %s\n", md.getRefbit()
    report_str+= "Pixel: %s\n", md.getPixel()
    report_str+= "Group: %s\n", md.getGroup()
    report_str+= "Parity: %s\n", md.getParity()
    report_str+= "Delay: %s\n",md.getDelay() 
    report_str+= "Dcolumn: %s\n",md.getDcolumn() 
    report_str+= "Bcid: %s\n",md.getBcid()
    report_str+= "Chipid: %s\n",md.getChipid() 
    report_str+= "Phase: %s\n",md.getPhase() 
    report_str+= "Evtid: %s\n",md.getEvtid() 
    report_str+= "Word1: %s\n", format(md.getWord1(),'b') 
    report_str+= "Word2: %s\n", format(md.getWord2() ,'b')
    report_str+= "Data length: %s\n", len(md.getData())
    report_str+= "Data: %s\n", md.getData()
    report_str+= "Data: %s\n", md.getData()[0:37]
    return report_str

signal.signal(signal.SIGINT, handler)

counter=0


print "Preparing data..... this can take a while"
data={}
out=0
for x in xrange(512):
    data[x]={}
    for y in xrange(512):
        data[x][y]={}
        for bit in xrange(37):
            if x> out+10 :
                print "\r{0:.0f} %".format((float(x)/512)*100)
                out=x
                pass
            #Create histogram
            name="Data x"+str(x)+"y"+str(y)+"bit"+str(bit)
            #print name
            data[x][y][bit]=ROOT.TH1F(name,"Dummy sender test;;",30,0,24)
            pass
        pass
    pass


##
print "Data prepared. Reading MALTA..."

counter=0


out=nevts/100

while counter < nevts:
    #progress output
    if counter> out+10 :
        print "\r {0:.0f} %".format((float(counter)/nevts)*100)
        out=nevts
        pass
    
    #pattern
    counter=counter+1 
    config=[]
    config.append(int('0b10000000',2)) # sharp pulses
    config.append(int('0b10000010',2)) 
    config.append(int('0b10000001',2))
    ipb.Write(0x8,config,True)
    
    #data
    d=ipb.Read(0x6, 2, True)
    md.setWord1(d[0])
    md.setWord2(d[1])
    md.unpack()
    datawords=[]
    datawords.append(md.getData()[0:37])
    #print datawords
    
    #get hits
    nhits=md.getNhits()
    
    allOk=True
    #get monitoring
    val_monitor=ipb.Read(0x7,37,True)
    if val_monitor[0] == 0:
        #Only happens when something is wrong
        print "val_monitor was 0"
        allOk=False
        pass
    
    if allOk==True:
        state=0
        number_pulses=0
        pulse1_len=0
        pulse2_len=0
        for hit in xrange(nhits):
            allOk=True
            hx=md.getRow(hit)
            hy=md.getColumn(hit)
            #Get widths
            for bit in xrange(31, 8, -1):#monitoring bits
                ch=0;
                # independent searches:
                #checking number of pulses
                if format(val_monitor[ch],'b')[bit] == "1" and state == 0: 
                    state=1
                    pass
                elif format(val_monitor[ch],'b')[bit]=="0" and state== 1:
                    number_pulses+=1
                    state=0
                    pass
                
                #checking for overtriggering
                if (bit == 24 or bit == 16) and state == 1:
                    overtrigger="Yes"
                    #if verbose: report_str+= "Overtrigger error in bit %s \n" % bit
                    pass
                
                #pulses length
                if format(val_monitor[ch],'b')[bit] == "1" : 
                    if number_pulses == 0: pulse1_len+=1
                    if number_pulses == 1: pulse2_len+=1
                    pass
                pass 
            #print "hx %s , hy %s, bit %s, pulse1_len:%s" % (hx, hy, bit, pulse1_len)
            data[hx][hy][bit].Fill(float(pulse1_len))
            #data[0][0][0].Fill(float(pulse1_len))
        pass
    pass


##



#print data 



time.ctime()
print "End time: %s" % time.strftime('%H:%M:%S  %b %d %Y\n')
print "Saving data...."
out=0
for x in data:
    if x> out+10 :
        print "{0:.0f} %\r".format((float(x)/512)*100)
        out=x
        pass
    for y in data[x]:
        for bit in data[x][y]:
            if x>500 and y > 500:
                data[x][y][bit].Write()
                pass
            pass
        pass
    pass

rf.Close()
print "Data saved."
print "End of script"
#raw_input("press enter to exit")