
#!/usr/bin/env python
####################################
## Daq script for MALTA read-out
# Dummy sender configuration values:
#
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# ignacio.asensi@cern.ch
# December 2017
####################################

import Herakles
import datetime
import argparse
import array
import time
import sys
import ROOT
import MaltaData  #output parsing  
import random
import os
import MaltaBase #configuration
import signal
from MaltaBase import *

parser=argparse.ArgumentParser()
parser.add_argument('-c','--conn_str',help='connection string',default="192.168.0.1")
parser.add_argument('-rf','--rootfile',help='ROOT output file. Not available yet',default="output.root")
parser.add_argument('-n','--nevts',help='Number of tests',type=int, required=True)
parser.add_argument('-v','--verbose',help='Verbose information',action='store_true')
parser.add_argument('-fa','--fake',help='Simulate data reading. Only for script development',action='store_true')
parser.add_argument('-r','--report',help='Report output file. Default is output/MaltaRODummySender_ignacio.log',default="output/MaltaRODummySender_ignacio.log")
parser.add_argument('-te','--terminal',help='Display output on terminal. Besides from output file.',action='store_true')
parser.add_argument('-ch0','--ch0',help='Print only channel 0. Good for large tests',action='store_true')
parser.add_argument('-root','--root',help='Display root histogram of pulses width',action='store_true')
#parser.add_argument('-t','--time',help='amount of time to read (s)',type=int,default=30)
#parser.add_argument('-pa','--patternAddr',help='Pattern Address',type=int,default=8)
#parser.add_argument('-pn','--patternNum',help='Pattern Number',type=str,default='0000')
#parser.add_argument('-rf','--resetFifo',help='Resets the FIFO',type=bool,default=False)

args=parser.parse_args()
verbose=False
verbose=args.verbose
nevts=args.nevts
fake=False
fake=args.fake
terminal=False
terminal=args.terminal
ch0=False
ch0=args.ch0
root = False
root=args.root
# dump file
time.ctime() # 'Mon Oct 18 13:35:29 2010'
time_str=time.strftime('%Y%m%d_%H%M%S')



logname="output/MaltaRODummySender_%s.log" % time_str
rootname="output/MaltaRODummySender_%s.root" % time_str

file = open(logname, 'a+')
report_str=''
report_str+= "\n"
report_str+="New test....................   "+ time.strftime('%H:%M:%S %b %d %Y\n')



#Connection
report_str+= "Connect to %s \n" % args.conn_str
ipb=Herakles.Uhal(args.conn_str)
if verbose: report_str+= "Connected\n" 
if verbose: report_str+= "Reseting FIFO\n"

md = MaltaData.MaltaData()
#fw = ROOT.TFile(args.file,"RECREATE")                    
h=ROOT.TH1F("Data","Dummy sender test;;",30,0,24)
h.GetXaxis().SetTitle("Pulse width")
h.GetYaxis().SetTitle("times")
t0 = time.clock()
t1 = t0
m_cont=True







#Error detection vars
reports=[] #for data-fifo differences
datareports={}
datareports["type1"]=[]       #type 1
datareports["missing"]=[]       #type 1
datareports["falsetrigger"]=[]  #type 2
datareports["overtrigger"]=[]   #type 3
monitoringreports={}
monitoringreports["nomonitoring"]=[]
allOk=True

def handler(signum, frame):
    report_str+= "You pressed ctr+c to quit\n"
    m_cont=False
    pass
def verboseData(self, data):
    report_str+= "refbit: %s\n", md.getRefbit()
    report_str+= "Pixel: %s\n", md.getPixel()
    report_str+= "Group: %s\n", md.getGroup()
    report_str+= "Parity: %s\n", md.getParity()
    report_str+= "Delay: %s\n",md.getDelay() 
    report_str+= "Dcolumn: %s\n",md.getDcolumn() 
    report_str+= "Bcid: %s\n",md.getBcid()
    report_str+= "Chipid: %s\n",md.getChipid() 
    report_str+= "Phase: %s\n",md.getPhase() 
    report_str+= "Evtid: %s\n",md.getEvtid() 
    report_str+= "Word1: %s\n", format(md.getWord1(),'b') 
    report_str+= "Word2: %s\n", format(md.getWord2() ,'b')
    report_str+= "Data length: %s\n", len(md.getData())
    report_str+= "Data: %s\n", md.getData()
    report_str+= "Data: %s\n", md.getData()[0:37]
    pass

signal.signal(signal.SIGINT, handler)

counter=0
report_str+= "Test starting...\n"
while counter < nevts:
    allOk=True
    if verbose: report_str+= "-------------------------------------------------------------\n"
    counter=counter+1 
    if m_cont==False: break
    ResetFifo(ipb)
    
    
    
    #error vars
    wrong_data=[]
    
    #
    if verbose: report_str+= "Test [%s].\n" % (counter)
    config=[]
    #configuring dummy sender 
    '''
    config.append(int('0b01110000',2)) # sharp pulses
    config.append(int('0b01110010',2)) 
    config.append(int('0b01110001',2))
    '''
    config.append(int('0b10000000',2)) # sharp pulses
    config.append(int('0b10000010',2)) 
    config.append(int('0b10000001',2))
    ipb.Write(0x8,config,True)
    
    #if verbose: report_str+= "Read data words"
    datawords=[]
    
    
    if fake:
        #fake reading
        wrong_data.append([])
        wrong_data.append([])
        data1=[0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        data2=[1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
        datawords.append(data1)
        datawords.append(data2)
        pass
    else:
        #real reading
        for x in range(5):
            data=ipb.Read(0x6, 2, True)
            md.setWord1(data[0])
            md.setWord2(data[1])
            md.unpack()
            #report_str+= "md.getData()[0]:",md.getData()[0]
            if md.getData()[0]== 0:break
            wrong_data.append([])
            datawords.append(md.getData()[0:37])
            if verbose: 
                report_str+= "Data: %s\n" % md.getData()[0:37]
                report_str+= "getWinid %s\n" % md.getWinid()
                pass
            pass
        pass
    
    #if verbose : report_str+= "datawords:", len(datawords)
   
    #if verbose: report_str+= "Read monitoring fifo"
    val_monitor=ipb.Read(0x7,37,True)
    minicount=0
    
    if ch0 == False:
        for ch in val_monitor:
            if verbose: report_str+= "%s | %s | %s | %s - %s\n" % (format(ch,'b')[0:8], format(ch,'b')[8:16], format(ch,'b')[16:24], format(ch,'b')[24:32], minicount)
            minicount+=1
            pass
        pass
    else:
        if verbose: report_str+= "%s | %s | %s | %s - %s\n" % (format(val_monitor[0],'b')[0:8], format(val_monitor[0],'b')[8:16], format(val_monitor[0],'b')[16:24], format(val_monitor[0],'b')[24:32], minicount)
        pass

    #if verbose: report_str+= "Align monitoring fifo"
    state=0# 0 -> no pulse, 1 -> pulse
    
    
    #check data words register same number of pulses. If yes then continue
    number_pulses=0
    dataword_n=0
    pulse_start=0
    if  len(datawords) == 0:
        allOk=False
        if verbose: report_str+= "no data no analysis :(\n" 
        datareports["type1"].append(counter)
        pass
    if val_monitor[0] == 0:
        #Only happens when something is wrong
        allOk=False
        monitoringreports["nomonitoring"].append(counter)
        report_str+= "Error: monitoring data is 0\n"
        print "No monitoring data!"
        pass
    if allOk == True:
        overtrigger="No"
        pulse1_len=0
        pulse2_len=0
        # finding pulses 
        for bit in xrange(31, 8, -1):#monitoring bits
            ch=0;
            # independent searches:
            #checking number of pulses
            if format(val_monitor[ch],'b')[bit] == "1" and state == 0: 
                state=1
                pass
            elif format(val_monitor[ch],'b')[bit]=="0" and state== 1:
                number_pulses+=1
                state=0
                pass
            
            #checking for overtriggering
            if (bit == 24 or bit == 16) and state == 1:
                overtrigger="Yes"
                datareports["overtrigger"].append(counter)
                if verbose: report_str+= "Overtrigger error in bit %s \n" % bit
                pass
            
            #pulses length
            if format(val_monitor[ch],'b')[bit] == "1" : 
                if number_pulses == 0: pulse1_len+=1
                if number_pulses == 1: pulse2_len+=1
                pass
            pass
        
        h.Fill(float(pulse1_len))
        
        
        if verbose: report_str+= "Number of pulses: %s.  number of datawords: %s.  Overtrigger: %s.  Length: %s\n" % (number_pulses, len(datawords), overtrigger, pulse1_len)
        if len(datawords) <  number_pulses:
            #if verbose: report_str+= "Unexpected error" # stop here
            if verbose: report_str+= "Error [Type 1]. Missing.\n" 
            datareports["type1"].append(counter)
            allOK=False
            pass
        
        if allOk == True:
            if len(datawords) == number_pulses:
                #OK
                pass
            if len(datawords) > number_pulses:
                if verbose: report_str+= "Possible False trigger.\n"
                for dataword in datawords:
                    #report_str+= "winid: %s" % dataword.getWinid()
                    pass
                pass 
            #Checking dataword and monitoring are equal. 
            for bit in xrange(31, 8, -1):#monitoring bits
                ch=0; 
                if format(val_monitor[ch],'b')[bit] == "1" and state == 0:
                    pulse_start=bit
                    for ch in xrange(37):
                        if str(datawords[dataword_n][ch])!=str(format(val_monitor[ch],'b')[bit]):
                            report_str+= "WRONG AT :     ch: %s, bit %s, monitor val: %s, data val %s, dataN: %s\n" % (ch, bit, format(val_monitor[ch],'b')[bit], datawords[dataword_n][ch], dataword_n)
                            wrong_data[dataword_n].append(ch)
                            pass
                        pass
                    state=1
                    pass
                elif format(val_monitor[ch],'b')[bit]=="0" and state== 1:
                    state=0
                    dataword_n+=1
                    #checking datawords registered as many pulses as monitoring
                    if len(datawords) < dataword_n+1:
                        break
                    pass
                pass
            
            
            reports.append(wrong_data)
            
            #if verbose: report_str+= "Wrong words"
            #report_str+= wrong_data;
            """
            Cases:
                did we find a single pulse in a single window
                ...
            """
            #    pass
            pass
        pass
    pass

report_str+= "End of test\n"
'''
ERRORS:
Ref. Bit 
    - missing         (type 1)
    - false positive  (type 2)
    - overtriggering  (type 3)
Fall word
    - missing
    - false positive
    - overtriggering
'''


# reports
report_str+= "\n"
report_str+= "Report:\n"
report_str+= "\n"
report_str+= "- Pulses width:\n"
report_str+= "--- Counted: %s.  Mean %s . Std dev: %s\n" % (h.GetEntries(), h.GetMean(), h.GetStdDev())
report_str+= "\n"




#type 1: data<monitoring and data=0
if len(datareports["type1"]) > 0:  
    prob=(float(len(datareports["type1"]))/float(nevts))*100
    report_str+= "- \"No data\" error: \t\t\t%s\t\t%s %% \n" % (len(datareports["type1"]), prob )
    if verbose:
        report_str+= "--- Loops: %s\n" % datareports["type1"]
        report_str+= "\n"
        pass
    pass

if len(datareports["missing"]) > 0:
    prob=(float(len(datareports["missing"])) / float(nevts))*100
    report_str+= "- Errors type 1. Missing: \t\t%s\t\t%s %%\n" % (len(datareports["missing"]), prob)
    if verbose:
        report_str+= "--- Loops: %s\n" % datareports["missing"]
        report_str+= "\n"
        pass
    pass

if len(datareports["falsetrigger"]) > 0:
    prob=(float(len(datareports["falsetrigger"])) / float(nevts))*100
    report_str+= "- Errors type 2. False trigger: \t%s\t\t%s %%\n" % (len(datareports["falsetrigger"]), prob)
    if verbose:
        report_str+= "--- Loops: %s\n" % datareports["falsetrigger"]
        report_str+= "\n"
        pass
    pass
    
if len(datareports["overtrigger"]) > 0:
    prob=(float(len(datareports["overtrigger"])) / float(nevts))*100
    report_str+= "- Errors type 3. Overtriggering: \t%s\t\t%s %%\n" % (len(datareports["overtrigger"]), prob)
    if verbose:
        report_str+= "--- Loops: %s\n" % datareports["overtrigger"]
        report_str+= "\n"
        pass
    pass

if len(monitoringreports["nomonitoring"]) > 0:
    prob=(float(len(monitoringreports["nomonitoring"])) / float(nevts))*100
    report_str+= "- Errors of monitoring \t\t\t%s\t\t%s %%\n" % (len(monitoringreports["nomonitoring"]), prob)
    if verbose:
        report_str+= "--- Loops: %s\n" % monitoringreports["nomonitoring"]
        report_str+= "\n"
        pass
    pass


report_str+= "End of report\n"

if terminal: print report_str
if root == True: h.Draw("colz")
h.SaveAs(rootname)
file.write(report_str)
file.close()
raw_input("press enter to exit")