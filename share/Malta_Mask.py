#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import os
import sys
import time
import argparse

import MaltaBase
from MaltaBase import *
import MaltaSetup
import Herakles
import PyMaltaSlowControl

parser=argparse.ArgumentParser()
parser.add_argument('-dc','--doublecolumn',help="List of double columns to mask",nargs='+',type=int)
parser.add_argument('-f','--first',help="First double column",type=int)
parser.add_argument('-l','--last',help="Last double column",type=int)
parser.add_argument('-s','--sample',help="Sample name")
parser.add_argument('-u','--unmask',help="Actually un-mask",action='store_true')
args=parser.parse_args()

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################

connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)

GetVersion(ipb,True)
print GetStatus(ipb)
getFifoState(ipb)
ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
os.system("MALTA_PSU.py -t T_SC_ON.txt ")

###################################
############# starting the SC clock
###################################
switchClockOn(ipb)

############# MaltaSlowControl instance
sc=PyMaltaSlowControl.MaltaSlowControl()

dcs=[]
if args.first and args.last:
    if args.first >= args.last:
        print "First DC cannot be smaller than last: %i %i" % (args.first, args.last)
        pass
    for dc in xrange(args.first,args.last+1):
        dcs.append(dc)
        pass
    pass

if args.doublecolumn:
    for dc in args.doublecolumn:
        if not dc in dcs: dcs.append(dc)
        pass
    pass

samples={"W12R3":[0,36],
         "W4R2":[3,47,57,77,80,81,82,121],
         "W4R10":[50,54,58,97,21,39,101,104],
         "W4R12":[9,15,19,26,79,98,101,110,112,121],
         "W4R20":[21,39,50,54,58,101,104],
         "W12R24":[4,16,46,53,54,55,58,69,70,86,104,118],
         "W12R11":[9,15,19,26,79,98,101,110,112,121],
         }        

if args.sample:
    if not args.sample in samples:
        print "Could not find sample: %s" % args.sample
        print "Available samples:"
        for s in samples: print "  %s" % s
        pass
    else:
        for dc in samples[args.sample]:
            if not dc in dcs: dcs.append(dc)
            pass
        pass
    pass

for dc in dcs:
    cmd=sc.maskDoubleColumn(dc)
    if args.unmask: cmd=sc.unmaskDoubleColumn(dc)
    print cmd
    ipb.Write(5, cmd)
    time.sleep(0.1)
    pass


###################################
############# stopping the SC clock
###################################
switchClockOff(ipb)

os.system("MALTA_PSU.py -t T_SC_OFF.txt ")
print "Have a nice day"
