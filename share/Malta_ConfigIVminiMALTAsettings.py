#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-DVDD' ,'--DVDD'       , help='DVDD when setting DAC, it will go to 1.9 when reading the DAC',type=float,default="0.9")
args=parser.parse_args()
chip=args.chip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(args.DVDD)+ " ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

readA =4
writeA=5
if args.dualchip:
    readA =90
    writeA=91


connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
#if chip==0:
#    ipb=Herakles.Uhal("udp://ep-ade-gw-01.cern.ch:50006")
#else:
#    ipb=Herakles.Uhal(connstr)
print ("Done")
GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure"
ipb.Write(writeA, sc.enableMerger(False))
#ipb.Write(writeA, sc.enableMerger(True))
ipb.Write(writeA, sc.delay(750)) #was 750
DebugWord(ipb.Read(readA), 0)

############# DAC monitoring
print "\n Disabling DAC monitoring"
ipb.Write(writeA, sc.switchDACMONV(False))
#ipb.Write(writeA, sc.switchDACMONV(True))
#ipb.Write(writeA, sc.switchDACMONV(True))
ipb.Write(writeA, sc.switchDACMONI(False))
ipb.Write(writeA, sc.readRegister(21)    )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
############# DAC override
print "\n Switching on IDB"
ipb.Write(writeA, sc.switchIDB(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ITHR"
ipb.Write(writeA, sc.switchITHR(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IBIAS"
ipb.Write(writeA, sc.switchIBIAS(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IRESET"
ipb.Write(writeA, sc.switchIRESET(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ICASN"
ipb.Write(writeA, sc.switchICASN(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCASN"
ipb.Write(writeA, sc.switchVCASN(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCLIP"
ipb.Write(writeA, sc.switchVCLIP(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_HIGH"
ipb.Write(writeA, sc.switchVPLSE_HIGH(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_LOW"
ipb.Write(writeA, sc.switchVPLSE_LOW(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_D"
ipb.Write(writeA, sc.switchVRESET_D(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_P"
ipb.Write(writeA, sc.switchVRESET_P(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)


##### SETTING miniMALTA values in MALTA ##########
print ("\n Setting ICASN ")
ipb.Write(writeA, sc.setICASN(10)) 
ipb.Write(writeA, sc.setICASN(10)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IBIAS ")
ipb.Write(writeA, sc.setIBIAS(50)) #50 miniMALTA
ipb.Write(writeA, sc.setIBIAS(50)) #50 miniMALTA
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting ITHR")
ipb.Write(writeA, sc.setITHR(20)) #WAS 1 
ipb.Write(writeA, sc.setITHR(20)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IDB")
ipb.Write(writeA, sc.setIDB(50)) 
ipb.Write(writeA, sc.setIDB(50)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IRESET")
ipb.Write(writeA, sc.setIRESET(127)) 
ipb.Write(writeA, sc.setIRESET(127)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VCASN")
ipb.Write(writeA, sc.setVCASN(64)) 
ipb.Write(writeA, sc.setVCASN(64)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VRESET_P")
ipb.Write(writeA, sc.setVRESET_P(127)) 
ipb.Write(writeA, sc.setVRESET_P(127)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VRESET_D")
ipb.Write(writeA, sc.setVRESET_D(58)) 
ipb.Write(writeA, sc.setVRESET_D(58)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_HIGH")
ipb.Write(writeA, sc.setVPLSE_HIGH(90)) 
ipb.Write(writeA, sc.setVPLSE_HIGH(90)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_LOW")
ipb.Write(writeA, sc.setVPLSE_LOW(5)) 
ipb.Write(writeA, sc.setVPLSE_LOW(5)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VCLIP")
ipb.Write(writeA, sc.setVCLIP(127)) 
ipb.Write(writeA, sc.setVCLIP(127)) 
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

#############
#ipb.Write(writeA, sc.setIBUFP_MON1(8) )
#ipb.Write(writeA, sc.setIBUFN_MON1(15) )
#ipb.Write(writeA, sc.setIBUFP_MON0(8) )
#ipb.Write(writeA, sc.setIBUFN_MON0(15) )
ipb.Write(writeA, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)


#############LVDS stuff
#print "\n LVDS register before change:"
#ipb.Write(writeA,sc.readRegister(5))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)
#ipb.Write(writeA,sc.configHBridge(31))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)
#print "\n LVDS register after change:"
#ipb.Write(writeA,sc.readRegister(5))
#DebugWord(ipb.Read(readA),0)
#time.sleep(0.1)

print "\n Configuring masking and pulsing"

############# VD: disable everything that can produce noise in the chip
#ipb.Write(writeA, sc.enablePulsePixelCol(10))
####ipb.Write(writeA, sc.enablePulsePixelCol(50))
#ipb.Write(writeA, sc.enablePulsePixelHor(10))
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
####ipb.Write(writeA, sc.enablePulsePixelHor(50))
print ("***Bob wants this EMPTY ***")
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
#time.sleep(0.1)
#ipb.Write(writeA, sc.enablePulsePixelCol(5))
#ipb.Write(writeA, sc.disablePulsePixelHor(0))
#ipb.Write(writeA, sc.disablePulsePixelHor(251))
#ipb.Write(writeA, sc.disablePulsePixelHor(511))
#for i in xrange(0, 5):
#   ipb.Write(writeA, sc.enablePulsePixelHor(1))
#   time.sleep(0.01)
#ipb.Write(writeA, sc.disablePulsePixelHor(1))
#ipb.Write(writeA, sc.disablePulsePixelHor(1))
#ipb.Write(writeA, sc.enablePulsePixelCol(444))
#ipb.Write(writeA, sc.enablePulsePixelHor(1))
#ipb.Write(writeA, sc.enablePulsePixelCol(50))
#ipb.Write(writeA, sc.enablePulsePixelHor(50))
#ipb.Write(writeA, sc.disablePulsePixelCol(5))
#ipb.Write(writeA, sc.disablePulsePixelHor(0))
#ipb.Write(writeA, sc.disablePulsePixelHor(251))
#ipb.Write(writeA, sc.disablePulsePixelHor(511))

'''
for i in range(0,512):
    ipb.Write( writeA,sc.maskPixelCol(i) )
    ipb.Write( writeA,sc.maskPixelCol(i) )
    ipb.Write( writeA,sc.maskPixelDiag(i) )
    ipb.Write( writeA,sc.maskPixelDiag(i) )
    time.sleep(0.001)

dcStart   =100
dcLength=  16

for i in range(0,dcStart):
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    time.sleep(0.001)

for i in range(dcStart+dcLength,256):
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    time.sleep(0.001)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)

rowStart=350
rowLength=130

for i in range(0,rowStart):
    ipb.Write( writeA,sc.maskPixelHor(i) )
    ipb.Write( writeA,sc.maskPixelHor(i) )
    time.sleep(0.001)

for i in range(rowStart+rowLength,512):
    ipb.Write( writeA,sc.maskPixelHor(i) )
    ipb.Write( writeA,sc.maskPixelHor(i) )
    time.sleep(0.001)
'''

# For chip W7R21 very noisy in column 255
"""
ipb.Write( writeA,sc.maskDoubleColumn(128))
ipb.Write( writeA,sc.maskDoubleColumn(128))
time.sleep(0.001)
ipb.Write( writeA,sc.maskDoubleColumn(127))
ipb.Write( writeA,sc.maskDoubleColumn(127))
time.sleep(0.001)
ipb.Write( writeA,sc.maskDoubleColumn(255))
ipb.Write( writeA,sc.maskDoubleColumn(255))
time.sleep(0.001)
"""
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)

switchClockOff(ipb)
print "\n DONE "
os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
