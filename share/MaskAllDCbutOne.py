#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import os
import sys
import time
import threading
import argparse

import MaltaBase
from MaltaBase import *
import MaltaSetup
import Herakles
import PyMaltaSlowControl

parser=argparse.ArgumentParser()
arser.add_argument('-c' ,'--column',help='double column not to mask',type=int,required=True)
args=parser.parse_args()

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################

connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)

GetVersion(ipb,True)
ResetFifo(ipb)
##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

################################################
############# starting the SC clock
switchClockOn(ipb)

################################################
############# MaltaSlowControl instance
sc=PyMaltaSlowControl.MaltaSlowControl()

dcol=int(arg.column/2)

for dc in xrange(0,93):
    theDC=93-dc
    if theDC==sc: continue
    command= sc.maskDoubleColumn(theDC)
    print command
    ipb.Write(5,command)
    time.sleep(0.01)
    ipb.Write(5, command)
    time.sleep(0.01)
    ipb.Write(5, command)
    time.sleep(0.01)
    pass


###################################
############# stopping the SC clock
###################################
time.sleep(0.2)
switchClockOff(ipb)
