#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################

parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
args=parser.parse_args()
chip=args.chip

#os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")


readA =4
writeA=5
if args.dualchip:
    readA =90
    writeA=91


connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)

GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)
time.sleep(0.1)
##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)
time.sleep(0.1)
ResetMalta(ipb)
time.sleep(0.121)
sc=PyMaltaSlowControl.MaltaSlowControl()

##########
print "Writing on IPBUS " +str(writeA)+ "(5:chip0   91:chip1)"
print "Reading on IPBUS " +str(readA)+ "(4:chip0   90:chip1)"

#######
############# disabling the merger

print  " read reg 21 " 
ipb.Write(writeA, sc.readRegister(21)    )
print  " SEND  readRegister21::::  "+str( format( (sc.readRegister(21)) , "016b" ) )
print  " Default value for register 21 :::: 4 / 001 0000 0000 0000  "
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
time.sleep(0.5)

print  " read reg 1 " 
ipb.Write(writeA, sc.readRegister(1)    )
print  " SEND  readRegister1::::  "+str( format( (sc.readRegister(1)) , "016b" ) )
print  " Default value for register 1 :::: 2829 / 1011 0000 1101 "
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
time.sleep(0.5)

print  " read reg 22 " 
ipb.Write(writeA, sc.readRegister(22)    )
print  " SEND  readRegister22::::  "+str( format( (sc.readRegister(22)) , "016b" ) )
print  " Default value for register 22 :::: 43690 / 1010 1010 1010 1010 "
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)
time.sleep(0.5)


switchClockOff(ipb)
print "\n DONE "

