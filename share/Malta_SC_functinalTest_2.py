#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse
from socket import gethostname

import MaltaSetup
import MaltaBase
from MaltaBase import *
import Herakles
import PyMaltaSlowControl
from asyncore import read
from plistlib import readPlist

readA =4
writeA=5

def printVal(value):
    val=""
    val+=" "+str( format( (value&0xFFFF) , "016b" ) )
    val+=" ::::   0x%04x"%(value&0xFFFF)
    return val

##################################################################################################
def DebugWord(word, attempt,ref=0):
    #time.sleep(0.001)
    #val="Attempt "+str(attempt) 
    val="Register: "+str( attempt)
    if word!=0: 
        val+="  NOT EMPTY "
        val+=" ::::  "+str( format( (word&0xFFFF) , "016b" ) )
        val+=" ::::   0x%04x"%(word&0xFFFF)
        val+=" ::::   %i"%(word&0xFFFF)
        if ref!=0:
            if ref!=(word&0xFFFF): val+="   <=========== "
    else      : val+="    EMPTY   "
    print val
    if word!=0: 
        return (word&0xFFFF)
    else:  
        return -1

##################################################################################################
def Read(val):
    ###val=h.Read(8)
    tmpList=list('{0:0b}'.format(val))
    print tmpList

##################################################################################################
def getFifoState(ip):
    value=ip.Read(3)
    print " "
    print "FIFOread  full : "+str( ( (value >> 8) & 0x1)!=0 )
    print "FIFOread  empty: "+str( ( (value >> 7) & 0x1)==0 )
    print "FIFOwrite  full: "+str( ( (value >>10) & 0x1)!=0 )
    print "FIFOwrite empty: "+str( ( (value >> 9) & 0x1)==0 )

#########################################################################################
def CMD_ResetMalta(herIP):
    ref = 0xFFFFFFFF
    bitIndex=12
    mask=ref ^ (1<<bitIndex)
    cacheWord= herIP.Read(8)
    herIP.Write(8, cacheWord | (1<<12) )
    time.sleep(1.0)
    herIP.Write(8, cacheWord & mask )
    print "DONE RESETTING MALTA"
    return 


##################################################################################################
def getAllRegisters(ipb):
    res=[]
    #for i in xrange(0,23):
    for i in xrange(0,32):
        time.sleep(0.02)
        w=int(sc.readRegister(i))#diccionario de comandos
        ###print printVal(w)
        ipb.Write(writeA, w)
        val1=ipb.Read(readA)
        res.append(val1&0xFFFF)
    return res

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
#connstr=MaltaSetup.MaltaSetup().getConnStr()
#print "Trying to connect to: "+connstr
#ipName=connstr
#ipb=Herakles.Uhal(ipName)
#GetVersion(ipb,True)
#print GetStatus(ipb)


parser = argparse.ArgumentParser()
parser.add_argument("-V","--verbose",help="verbose mode",action='store_true')
#parser.add_argument("--n",help="Reading N a register", required=True)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
args=parser.parse_args()
chip=args.chip

os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(0.8)+ " ")

connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)

#ResetFifo(ipb)
getFifoState(ipb)

chip=args.chip
if args.dualchip:
    readA =90
    writeA=91

##sys.exit()

switchClockOn(ipb)

C_all    =0.
C_correct=0.
C_wrong  =0.
##########################################ipb.Write(writeA,0x6000) #disable merger
### writing what we think is the default configuration

DebugWord(ipb.Read(readA),0)
DebugWord(ipb.Read(readA),0)
DebugWord(ipb.Read(readA),0)
print " "
print " "

### a bunch of self test
for i in xrange(0,10): #####
    C_all+=1.
    print " "+str(i)
    time.sleep(0.05)
    ipb.Write(writeA,0xA000)
    #time.sleep(1)
    #ipb.Write(writeA,0x8010)
    ##ipb.Write(writeA,0x8150)
    v1=DebugWord(ipb.Read(readA),0)
    #time.sleep(1)
    v2=DebugWord(ipb.Read(readA),1)
    if v2==-1 and v1==43690: C_correct+=1.
    ###if v2==-1 and v1==2828 : C_correct+=1.
    else                   : C_wrong  +=1.

print " " 
print " " 
print "SUMMARY: "
print "  -- sent tests: "+str(C_all)+"  ---> CORRECT results: "+str(C_correct)+" === "+str( C_correct/C_all*100)+" %"
print "#########################################################################################################"
print " "

####ipb.Write(8, cacheWord & mask)
####### UP TO HERE IS MANDATORY!!!! #############################################################
sc=PyMaltaSlowControl.MaltaSlowControl()

#nread=args.n
verbose=args.verbose
print "Configuration:"
#print "Reading %s each register" % nread
print "Mode verbose %s" % verbose

#is important to know if val 2 is not 0!!
#store vals of 1 and then write a report. Look the first 16 bits of the words. The last bit is always 1. how many times each value

ResetFifo(ipb)
defaultResults=getAllRegisters(ipb)
print " "
print defaultResults
print " "
defaultResults [12] = 1
defaultResults [13] = 1
defaultResults [14] = 1
defaultResults [15] = 1
defaultResults [16] = 1


commands=[
 ##   [ 2, sc.enablePowerSwitch(False,False),sc.enablePowerSwitch(True,True)],
#    [ 3, sc.enableLVDS(False), sc.enableLVDS(True)],
  ##  [ 4, sc.configPreEmphasis("15=>10", 1), sc.configPreEmphasis("15=>10", 18) ],
#    [ 4, 0x513f, 0x5100],
#    [ 5, sc.configHBridge(10), sc.configHBridge(28)],
#    [ 6, sc.configCMFB(1)    , sc.configCMFB(16)],
#    [ 7, sc.configIBCMFB(10) , sc.configIBCMFB(7)],
#    [ 8, sc.configIVPH(10)   , sc.configIVPH(2)],
#    [ 9, sc.configIVPL(9)    , sc.configIVPL(13)],
#    [10, sc.configIVNH(11)   , sc.configIVNH(13)],      
#    [11, sc.configIVNL(10)   , sc.configIVNL(7)],      
    [12, sc.writeReservedRegister(0, 5), sc.writeReservedRegister(0, defaultResults [12])],
    [13, sc.writeReservedRegister(1, 6), sc.writeReservedRegister(1, defaultResults [13])],
    [14, sc.writeReservedRegister(2, 7), sc.writeReservedRegister(2, defaultResults [14])],
    [15, sc.writeReservedRegister(3, 8), sc.writeReservedRegister(3, defaultResults [15])],
    [16, sc.writeReservedRegister(4, 9), sc.writeReservedRegister(4, defaultResults [16])],
#    [17, sc.setIBUFP_MON0(1)           , sc.setIBUFP_MON0(5) ],
#    [18, sc.setIBUFN_MON0(2)           , sc.setIBUFN_MON0(9) ],
#    [19, sc.setIBUFP_MON1(3)           , sc.setIBUFP_MON1(5) ],
#    [20, sc.setIBUFN_MON1(2)           , sc.setIBUFN_MON1(9) ],
    ]
for l in commands:
    print l



for val in commands:
    #####CMD_ResetMalta(ipb)
    print " "
    print " "
    print "-------------------------------------------------------------"
    print "Testing register : "+str(val[0])
   # print "The default values are: \n",defaultResults
    print " "
    count=-1
    for f in val:
        time.sleep(0.1)
        count+=1
        if count==0 or count == 1: continue
        ipb.Write(writeA,f)
        tmpV=ipb.Read(readA)&0xFFFF
        if tmpV!=0: 
            #print (" COMMAND: 0x%04x"%(f))+(" .... CAME BACK as 0x%04x "%(tmpV&0xFFFF))
            print " COMMAND: "+str( format( (f&0xFFFF) , "016b" ) )+"         returning as: "+str( format( (tmpV&0xFFFF) , "016b" ) )
        ####DebugWord(ipb.Read(readA),0)
        '''
       else: 

            res =  getAllRegisters(ipb)
            if count == 1:

              print res
              print res[12] == 0
              print res[13] == 0
              print res[14] == 0
              print res[15] == 0
              print res[16] == 0
            if count == 2:
              print res
              print res[12] == 0
              print res[13] == 0
              print res[14] == 0
              print res[15] == 0
              print res[16] == 0
'''
#wait forever, then read
print'Sleeping'
time.sleep(1)
res = getAllRegisters(ipb)

print defaultResults
print res

print res == defaultResults





switchClockOff(ipb)


os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")

output=""
outputErr=""
'''
#reading results
numberErr=0
count=-1
for reg in values:
    count+=1
    entries=len(reg)
    print " "
    print " - reg: "+str(count)+" HAD "+str(entries)+" TEST: "
    results=[]
    for v in reg:
        if len(results)==0: 
            results.append( [v,1] )
            continue
        found=False
        for l in results:
            if v==l[0]: 
                l[1]+=1
                found=True
        if not found:
            results.append( [v,1] )
    print results
'''
print " "
print "End of script.\n"














